 <?php get_header(); ?>
 <div role="main">
    <section class="project wrap">

        <header>
            <h2>The intersection</h2>
            <p>Shifting expectations of todays youth</p>
        </header>

        <div class="project__screenshots grids">
            <span class="grid-12">
                <img src="http://www.placehold.it/940x500">
            </span>
            <span class="grid-6"><img src="http://www.placehold.it/500x300"></span>
            <span class="grid-6"><img src="http://www.placehold.it/600x300"></span>
        </div>

        <section class="project__info grids">
            <div class="summary grid-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>  </div>
            <div class="info grid-4">
                <p>info</p>
            </div>
        </section>

    </section>
</div>
<?php get_footer(); ?>