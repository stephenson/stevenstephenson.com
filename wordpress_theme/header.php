<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
<title><?php
    /*
     * Print the <title> tag based on what is being viewed.
     */
    global $page, $paged;

    wp_title('|', true, 'right');

    // Add the blog name.
    bloginfo('name');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display');
    if ( $site_description && (is_home() || is_front_page()))
        echo " | $site_description";

    // Add a page number if necessary:
    if ( $paged >= 2 || $page >= 2 )
        echo ' | ' . sprintf('Page %s', max( $paged, $page ) );

    ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script src="<?php bloginfo('template_directory') ?>/_/j/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="http://use.edgefonts.net/league-gothic.js"></script>
<script src="http://use.edgefonts.net/merriweather.js"></script>
<script src="http://use.edgefonts.net/open-sans.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
<header role="banner">
    <div class="wrap">
        <hgroup class="branding">
            <div class="logo">
                <a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_directory') ?>/_/i/logo.png"></a>
            </div> 
        </hgroup>

        <nav role="navigation">
            <ul>
                <li><a href="/journal">Journal</a></li>
                <li><a href="/info">Info</a></li>
                <li><a href="http://www.imagekind.com/GalleryProfile.aspx?gid=5d563953-563e-4cc5-a555-867a5aa23c9c" target="_blank">Shop</a></li>
            </ul>
        </nav>
    </div>
</header><!-- end role[banner] -->