<?php 
/*
Template Name: Info
*/
get_header(); ?>
<div role="main" class="wrap">
    <section class="grids">
      <div class="grid-4 info__img">
        <img src="http://www.stevenstephenson.com/wp-content/uploads/2013/02/jpeg.jpeg">
      </div>
      <section class="grid-8 info__bio">

        <h3>How can I sum this up... I'm an artist at heart with a deep passion for development and design</h3>
        <p>Last month I received my Masters Degree in Applied Information Technology with a concentration in Database Management Systems & Web Applications Development (Horray Me!).</p>
        <p>When not thinking about the web I enjoy watching weird movies, reading good books, watching addictive TV shows and learning new things.</p>
        <section class="skills">
            <h4>Skills (that kill)</h4>
            <div class="grid-5">
                <h5>Shogun Status</h5>
                <ul>
                    <li>HTML5</li>
                    <li>CSS 2.1 - 3</li>                           
                    <li>Status</li>                           
                    <li>JQuery</li>                             
                    <li>Javascript</li>                             
                    <li>Database Design</li>                           
                    <li>MySQL</li>
                    <li>ColdFusion</li>
                    <li>Drupal</li>
                    <li>WordPress</li>                     
                    <li>ExpressionEngine</li>                   
                    <li>Web Design</li>                   
                    <li>Responsive Web Design (Mobile First)</li>                   
                    <li>Logo Design</li>                   
                </ul>
            </div>
            <div class="grid-5">
                <h5>On the road to mastery</h5>
                <ul>
                    <li>PHP</li>
                    <li>CodeIgnitor</li>
                    <li>Coffeescript</li>
                    <li>Mangodb</li>
                    <li>Ruby on Rails</li>
                </ul>
            </div>
        </section>

      </section>

    </section>
</div>

<?php get_footer(); ?>
