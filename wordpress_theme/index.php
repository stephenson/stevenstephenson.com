<?php get_header(); ?>
<div role="main" class="wrap">
<section class="grids">
    <?php if (have_posts()) : ?>  
        <?php while (have_posts()) : the_post(); ?>  
        <article class="post grid-9" id="post-<?php the_ID(); ?>">
            <header>
                <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to 
            <?php the_title(); ?>"><?php the_title(); ?></a></h4>
               
                <time><?php the_time('F jS, Y') ?> <!-- by <!--?php the_author() ?--></time>  
                  
            </header>
            
            <section class="content">
                <?php the_excerpt('Read the rest of this entry »'); ?>  
            
              
            <p class="postmetadata">Posted in <?php the_category(', ') ?> <strong>|</strong>  
                <?php edit_post_link('Edit','','<strong>|</strong>'); ?>  
            </p>  
            </section>  
        </article>  
        <?php endwhile; ?>  
        <div class="navigation">  
            <div class="alignleft"><?php next_posts_link('« Previous Entries') ?></div>  
            <div class="alignright"><?php previous_posts_link('Next Entries »') ?></div>  
        </div>  
    <?php else : ?>  
        <article class="post grid-9" id="post-<?php the_ID(); ?>">
        <h2 class="center">Not Found</h2>  
        <p class="center">Sorry, but you are looking for something that isn't here.</p>  
        <?php include (TEMPLATEPATH . "/searchform.php"); ?>  
        </article>
    <?php endif; ?> 
</section>
</div>
<?php get_footer(); ?>
