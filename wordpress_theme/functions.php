<?php

add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'work-image', 800, 9999 ); //300 pixels wide (and unlimited height)
	//add_image_size( 'homepage-thumb', 220, 180, true ); //(cropped)
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'work',
		array(
			'labels' => array(
				'name' => __( 'Work' ),
				'singular_name' => __( 'Work' )
			),
		'public' => true,
		'has_archive' => false,
		'capability_type' => 'page',
		'supports'           => array('title','thumbnail','custom-fields', 'editor')
		)
	);
}

