<footer role="contentinfo">
    <div class="wrap">
        <p><a href="mailto:hello@stevenstephenson.com">hello{at}stevenstephenson.com</a> &bull; <a href="https://twitter.com/creativeowtlaw">@creativeowtlaw</a></p>
        <small>Copyright  &copy; Steven Stephenson, <?php echo date('Y') ?></small>
    </div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/_/j/plugins.js"></script>
<script src="<?php bloginfo('template_directory') ?>/_/j/main.js"></script>

<script>
    var _gaq=[['_setAccount','UA-12387202-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
<?php wp_footer(); ?>
</body>
</html>
