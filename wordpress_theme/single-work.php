 <?php get_header(); ?>
 <div role="main">
    <section class="project wrap">

        <header>
            <h2><?php the_title(); ?></h2>
            <p><?php echo the_field('short_description'); ?></p>
        </header>

        <div class="project__screenshots grids">
            <?php $images = get_field('work_samples');
            
            if( $images ): ?>
            <?php foreach($images as $image) { ?>    
                <span class="<?php echo $image['grid_width'] ?>"><img src="<?php echo $image['image'] ?>"></span>
            <?php } endif; ?> 
            
        </div>

        <section class="project__info grids">
        <?php if (have_posts()) : ?>  
            <?php while (have_posts()) : the_post(); ?>  
            <div class="summary grid-8">
                <?php the_content(); ?>
            </div>
        <?php endwhile; endif; ?>
            <div class="info grid-4">
                <div class="view-section">
                <?php if(get_field('work_url')): ?>
                    <a class="btn" href="<?php echo the_field('work_url')?>">View the site</a>
                    <?php endif; ?>
                </div>
                <h4>Role</h4>
                <ul>
                <?php $roles = get_field('what_i_did');
                
                if( $roles ): ?>
                <?php foreach($roles as $role) { ?>    
                    <li><?php echo $role['responsibility'] ?></li>
                <?php } endif; ?> 
                </ul>
            </div>
            
        </section>

    </section>
</div>
<?php get_footer(); ?>