<?php 
/*
Template Name: Home
*/

get_header(); ?>

<section class="highlight">

    <div class="wrap">
        <p class="bio">
            <?php the_field('introduction_text'); ?>
        </p>
    </div>

</section>

<section class="work">

    <header>
        <h2>Work</h2>
    </header>
    <div class="wrap">
    <section class="work__samples grids">
        <?php
        if ( get_query_var('paged') ) $paged = get_query_var('paged');  
        if ( get_query_var('page') ) $paged = get_query_var('page');
         
        $query = new WP_Query( array( 'post_type' => 'work', 'paged' => $paged ) );
         
        if ( $query->have_posts() ) : ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                <figure class="grid-4">
                    <?php the_post_thumbnail(); ?>
                    <figcaption>
                        <h4><?php the_title(); ?></h4>
                        <p><?php echo the_field("short_description"); ?></p>
                        <a href="<?php the_permalink(); ?>" class="btn">view</a>
                    </figcaption>
                </figure><!---  end fig --> 
                
        	<?php endwhile; wp_reset_postdata(); ?>
        	<!-- show pagination here -->
        <?php else : ?>
        	<!-- show 404 error here -->
        <?php endif; ?>
        </div>
    </section>
</section><!-- .work -->

<section class="hfeed home-blog-feed wrap">
    <div class="grids">
        <header class="grid-12">
            <h2>Blog</h2>
        </header>
        <?php global $query_string; // required
        $posts = query_posts($query_string.'&posts_per_page=2&order=ASC'); ?>
        
        <?php // DEFAULT LOOP GOES HERE ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
         <article class="grid-6 entry">
            <h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h4>
            <time><?php the_time('F jS, Y') ?> </time>
            <?php the_excerpt(); ?>
        </article>
        
        <?php endwhile; else: ?>
            <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
        
        <?php wp_reset_query(); // reset the query ?>
    </div>
</section>

<?php get_footer(); ?>
