$(document).ready(function() {
    
    $('figure img').attr('width', ' ').attr('height', ' ');
    
    $('.work__samples figure').hover(
        function(){
            $(this).children('figcaption').fadeIn(); 
        }, function () {
            $(this).children('figcaption').hide();
        }
    );
    
});
